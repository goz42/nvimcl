return {
  "HiPhish/rainbow-delimiters.nvim",
  lazy = false,
  config = function()
    require("colorizer").setup({})
  end,
}
