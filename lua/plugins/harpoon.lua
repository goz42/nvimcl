local M = {
  "ThePrimeagen/harpoon",
  event = "VeryLazy",
  dependencies = {
    { "nvim-lua/plenary.nvim" },
  },
}

function M.config()
  local keymap = vim.keymap.set
  local opts = { noremap = true, silent = true }
  local harpoon = require("harpoon")

  keymap("n", "<TAB>", "<cmd>lua require('harpoon.ui').toggle_quick_menu()<cr>", opts)
  keymap("n", "<leader>m", "<cmd>lua require('harpoon.mark').add_file()<cr>", opts)
  keymap("n", "<leader>hh", "<cmd>lua require('harpoon.ui').nav_prev()<cr>", opts)
  keymap("n", "<leader>hp", "<cmd>lua require('harpoon.ui').nav_prev()<cr>", opts)
  keymap("n", "<leader>hl", "<cmd>lua require('harpoon.ui').nav_next()<cr>", opts)
  keymap("n", "<leader>hn", "<cmd>lua require('harpoon.ui').nav_next()<cr>", opts)
  keymap("n", "<leader>h1", "<cmd>lua require('harpoon.ui').nav_file(1)<cr>", opts)
  keymap("n", "<leader>h2", "<cmd>lua require('harpoon.ui').nav_file(2)<cr>", opts)
  keymap("n", "<leader>h3", "<cmd>lua require('harpoon.ui').nav_file(3)<cr>", opts)
  keymap("n", "<leader>h4", "<cmd>lua require('harpoon.ui').nav_file(4)<cr>", opts)
  vim.api.nvim_create_autocmd({ "filetype" }, {
    pattern = "harpoon",
    callback = function()
      vim.cmd([[highlight link HarpoonBorder TelescopeBorder]])
      -- vim.cmd [[setlocal nonumber]]
      -- vim.cmd [[highlight HarpoonWindow guibg=#313132]]
    end,
  })
end

function M.mark_file()
  require("harpoon.mark").add_file()
  vim.notify("󱡅  marked file")
end

return M
