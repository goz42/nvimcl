return {
  {
    "catppuccin/nvim",
    lazy = false,
    name = "catppuccin",
    -- you can do it like this with a config function
    config = function()
      require("catppuccin").setup({
        flavour = "frappe",
        transparent_background = false,
        -- transparent_background = true,
        styles = { -- Handles the styles of general hi groups (see `:h highlight-args`):
          comments = { "italic" }, -- Change the style of comments
          conditionals = { "italic" },
          loops = {},
          functions = { "italic", "bold" },
          -- functions = { "reverse", "italic", "bold" },
          keywords = { "bold" },
          strings = {},
          variables = { "italic" },
          numbers = {},
          booleans = {},
          properties = {},
          types = {},
          operators = {},
        },
      })
    end,
    -- or just use opts table
    opts = {
      -- configurations
    },
  },
}
