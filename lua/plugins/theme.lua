return {
  "LazyVim/LazyVim",
  opts = {
    colorscheme = "catppuccin-frappe",
    -- colorscheme = "carbonfox",
    -- colorscheme = "tokyonight-moon",
    -- colorscheme = "palenight",
    -- colorscheme = "nightfox",
    -- colorscheme = "onenord",
    -- colorscheme = "rose-pine",
    -- colorscheme = "kanagawa",
    -- colorscheme = "onedark",
  },
}
