return {
  {
    "rmehri01/onenord.nvim",
    lazy = false,
    -- you can do it like this with a config function
    config = function()
      require("onenord").setup({
        -- theme = dark,
        styles = {
          comments = "italic",
          keywords = "bold",
          functions = "italic",
          variables = "bold",
        },
        transparent_background = true,
      })
    end,
    -- or just use opts table
    opts = {
      -- configurations
    },
  },
}
