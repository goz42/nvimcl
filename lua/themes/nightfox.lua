return {
  {
    "EdenEast/nightfox.nvim",
    lazy = false,
    -- you can do it like this with a config function
    config = function()
      require("nightfox").setup({
        options = {
          transparent = true, -- Disable setting background
          styles = { -- Style to be applied to different syntax groups
            comments = "italic", -- Value is any valid attr-list value `:help attr-list`
            conditionals = "NONE",
            constants = "NONE",
            functions = "italic,bold",
            keywords = "italic,bold",
            numbers = "NONE",
            operators = "italic",
            strings = "NONE",
            types = "italic,bold",
            variables = "bold",
          },
        },
      })
    end,
    -- or just use opts table
    opts = {
      -- configurations
    },
  },
}
