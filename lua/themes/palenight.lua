return {
  {
    "alexmozaidze/palenight.nvim",
    lazy = false,
    -- you can do it like this with a config function
    config = function()
      require("palenight").setup({
        italic = true,
        -- transparent_background = true,
      })
    end,
    -- or just use opts table
    opts = {
      -- configurations
    },
  },
}
