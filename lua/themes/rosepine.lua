return {
  "rose-pine/neovim",
  name = "rose-pine",
  lazy = false,
  -- you can do it like this with a config function
  config = function()
    require("rose-pine").setup({
      variant = "moon", -- auto, main, moon, or dawn
      dark_variant = "moon", -- main, moon, or dawn
      dim_inactive_windows = false,
      extend_background_behind_borders = true,

      styles = {
        bold = true,
        italic = true,
        transparency = true,
      },
    })
  end,
  -- or just use opts table
  opts = {
    -- configurations
  },
}
